package com.p36.udi.udicore;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.slf4j.Logger;

import com.sap.cloud.sdk.cloudplatform.exception.ObjectLookupFailedException;
import com.sap.cloud.sdk.cloudplatform.logging.CloudLoggerFactory;
import com.sap.cloud.sdk.cloudplatform.naming.JndiLookupAccessor;

public class ServletContext implements ServletContextListener{
	
	private static Logger Logger = CloudLoggerFactory.getLogger(ServletContext.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		Logger.info("App started");
		
		// Logon to database
		
		Map<String, DataSource> connectionMap = createMultiTenantProperties("PROVIDER");
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("sample", connectionMap);
		EntityManager em = emf.createEntityManager();
		
		if(em != null){
			Logger.info("Em created.");
		}else{
			Logger.info("Em not created");
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		Logger.info("App stopped");
	}
	
	
	private Map<String, DataSource> createMultiTenantProperties(String tenantId) {
		HashMap<String, DataSource> propertiesMap = new HashMap<String, DataSource>();

		DataSource ds;
		try {
			ds = (DataSource) JndiLookupAccessor.lookup("unmanageddatasource:" + tenantId);
			propertiesMap.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, ds);
		} catch (ObjectLookupFailedException e) {
			Logger.error("Lookup of datasource " + tenantId + " failed.");
			return null;
		}
		return propertiesMap;
	}
}